package com.zeyu.framework.tools.report.convert.pool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 对象池实现
 * Created by zeyuphoenix on 16/9/3.
 */
public class BlockingQueuePool<T> extends AbstractPool<T> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // 线程安全的队列
    LinkedBlockingQueue<T> linkQueue;

    // ================================================================
    // Constructors
    // ================================================================

    /**
     * 构造函数
     */
    public BlockingQueuePool(ObjectFactory<T> factory, int number, int maxWait,
                             long retentionTime) throws PoolException {
        super(factory, number, maxWait, retentionTime);
        queue = new LinkedBlockingQueue<>();
        linkQueue = (LinkedBlockingQueue<T>) queue;
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public T borrowObject() throws InterruptedException, PoolException {
        T object = linkQueue.poll(maxWait, TimeUnit.MILLISECONDS);
        if (object == null) {
            throw new PoolException();
        }
        poolSize.getAndDecrement();

        return object;
    }

    @Override
    public void returnObject(T object, boolean validate) throws InterruptedException {
        if (object == null) {
            return;
        }

        boolean valid = (!validate || objectFactory.validate(object));

        if (!valid) {
            destroyObject(object);
        } else {
            objectFactory.passivate(object);
            linkQueue.offer(object, maxWait, TimeUnit.MILLISECONDS);
            poolSize.incrementAndGet();
        }
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
