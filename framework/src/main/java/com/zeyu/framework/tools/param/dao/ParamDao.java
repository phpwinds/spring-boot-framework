package com.zeyu.framework.tools.param.dao;


import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.TreeDao;
import com.zeyu.framework.tools.param.entity.Param;

/**
 * 参数配置DAO接口
 */
@MyBatisDao
public interface ParamDao extends TreeDao<Param> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================
}
