package com.zeyu.framework.tools.report;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.zeyu.framework.core.web.BaseController;
import com.zeyu.framework.tools.report.charts.ChartData;
import com.zeyu.framework.tools.report.charts.ChartUtils;
import com.zeyu.framework.tools.report.charts.ExtendChartData;
import com.zeyu.framework.tools.report.convert.Converter;
import com.zeyu.framework.tools.report.dynamic.ReportGenerator;
import com.zeyu.framework.tools.report.dynamic.ReportUtils;
import net.sf.dynamicreports.report.builder.component.ComponentBuilder;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.ImageIcon;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * 整体报表展示和配置页面,目前仅仅进行测试
 * Created by zeyuphoenix on 16/9/5.
 */
@Controller
@RequestMapping(value = "/tools/report")
public class ReportController extends BaseController {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 报表页面
     */
    @RequestMapping(value = {"index", ""})
    public String index() {

        return "modules/tools/report";
    }


    @ResponseBody
    @RequestMapping(value = "preview")
    public Map<String, String> preview(boolean base64, String params) {

        Map<String, String> content = Maps.newHashMap();
        // 解析参数,以后使用构造chart data
        String res;
        try {
            if (base64) {
                res = new String(Base64.decodeBase64(params), "UTF-8");
            } else {
                res = params;
            }
            // 可以通过json mapper解码,变为map对象
            logger.info("report params info is: {}", res);

            ExtendChartData extendChartData = createChartData();
            List<ComponentBuilder<?, ?>> builders = Lists.newArrayList();

            // convert
            String output = Converter.getInstance().convert(extendChartData);

            byte[] bytes = ChartUtils.generateImage(output);
            logger.debug("out bytes: ", bytes);

            ImageIcon icon = new ImageIcon(bytes);
            logger.debug("icon is: ", icon);

            // //////////////////////////////////////////
            // create
            // //////////////////////////////////////////

            ComponentBuilder<?, ?> builder = ReportGenerator.createImageComponent("用户流量统计图",
                    icon);
            builders.add(builder);
            builder = ReportGenerator.createTitleComponent("信息信息");
            builders.add(builder);
            builder = ReportGenerator.createTableComponent("用户流量统计表", ReportGenerator.createDataSource(extendChartData));
            builders.add(builder);

            ComponentBuilder<?, ?>[] buildArray = new ComponentBuilder<?, ?>[builders.size()];
            builders.toArray(buildArray);
            URL url = ReportController.class.getClassLoader().getResource("tmp/generate");

            if (url != null) {
                String path = url.getPath() + File.separator + "用户流量.pdf";
                ReportGenerator.buildPDF("用户流量信息图", path, buildArray);

                content.put("pdfData", ReportUtils.BASE64_MARKER + ReportUtils.pdfToBase64(new File(path)));
            }

        } catch (Exception e) {
            logger.error("",e);
        }
        return content;
    }


    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

    /**
     * 创建图数据,只是测试使用,正式使用时,需要从数据库取得
     */
    private ExtendChartData createChartData() {
        // 图样式和数据对象
        ExtendChartData extendChartData = new ExtendChartData();
        // 图数据对象
        ChartData datas = new ChartData();

        // xAxis
        List<String> xAxis = Lists.newArrayList();
        CollectionUtils.addAll(xAxis, new String[]{"一月", "二月", "三月", "四月",
                "五月", "六月", "七月", "八月", "九月"});

        // series
        List<List<Number>> series = Lists.newArrayList();
        List<Number> serie1 = Lists.newArrayList();
        CollectionUtils.addAll(serie1, new Number[]{1200, 2320, 301000000, 434,
                39000, 53000000, 32000000, 120, 250});
        List<Number> serie2 = Lists.newArrayList();
        CollectionUtils.addAll(serie2, new Number[]{15000, 100060, 100090, 150,
                2000032, 20000001, 1000054, 390, 10000010});

        series.add(serie1);
        series.add(serie2);

        // inner
        datas.setxAxis(xAxis);
        datas.setSeries(series);

        // basic info.
        extendChartData.setRemote(false);
        extendChartData.setType("line");
        extendChartData.setTilte("用户流量趋势图");
        List<String> legends = Lists.newArrayList();
        legends.add("上行流量");
        legends.add("下行流量");
        // legends
        extendChartData.setLegends(legends);
        extendChartData.setDatas(datas);

        return extendChartData;
    }

}
