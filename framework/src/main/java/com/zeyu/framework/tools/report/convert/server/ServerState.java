package com.zeyu.framework.tools.report.convert.server;

/**
 * 转换服务的状态
 * Created by zeyuphoenix on 16/9/3.
 */
public enum ServerState {

    // ================================================================
    // Constants
    // ================================================================

    IDLE, DEAD, BUSY, TIMEDOUT, ACTIVE;

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

}
