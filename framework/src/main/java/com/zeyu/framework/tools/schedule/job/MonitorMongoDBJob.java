package com.zeyu.framework.tools.schedule.job;

import com.zeyu.framework.monitors.mongodb.MongoDBCollector;
import com.zeyu.framework.tools.schedule.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * quartz simple job, can like it.
 * Created by zeyuphoenix on 16/6/20.
 */
public class MonitorMongoDBJob extends AbstractJob {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private MongoDBCollector mongoDBCollector;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void process() throws Exception {
        logger.debug("执行mongodb数据库监控开始");
        // do self work in here.
        mongoDBCollector.collect();
        logger.debug("执行mongodb数据库监控结束");
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
