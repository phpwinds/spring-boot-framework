package com.zeyu.framework.tools.mq.receiver;

import com.zeyu.framework.core.configuration.RabbitMqConfiguration;
import com.zeyu.framework.tools.mq.struct.MQMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * MQ消息的统一处理器,基于方法的处理,可以处理不同信道的消息
 * Created by zeyuphoenix on 2016/12/31.
 */
@Component
public class MessageListener {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(MessageListener.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // 一个queue的一种消息,只有一个可以接收
    @RabbitListener(queues = RabbitMqConfiguration.QUEUE_COMMON) //启用Rabbit队列监听common
    public void process(@Payload MQMessage message) {
        logger.info("接收到common通道的信息: " + message);
    }

    // 发布订阅通道监听
    @RabbitListener(queues = RabbitMqConfiguration.QUEUE_FANOUT) //启用Rabbit队列监听fanout
    public void fanout(@Payload MQMessage message) {
        logger.info("信道接收发布定阅信息: " + message);
    }

    // 发布订阅通道监听
    @RabbitListener(queues = RabbitMqConfiguration.QUEUE_FANOUT_SEC) //启用Rabbit队列监听fanout sec
    public void fanout2(@Payload MQMessage message) {
        logger.info("另一信道同时接收发布订阅信息: " + message);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
