package com.zeyu.framework.tools.cron.entity;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Cron页面请求的参数
 *
 * <pre>
 * 1.新建
 * 	 cronExpression为空
 *   cronOptions为页面选择项
 * 2.修改
 * 	 cronExpression为正常的cron表达式
 *   cronOptions为空
 * </pre>
 *
 * @author zeyuphoenix
 */
public class CronCriterias implements Serializable, CronConst {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /** 生产的cron表达式，类似 0 0/5 * * * ? 格式. */
    /** 新建时是空，修改时有值 */
    private String cronExpression;

    /** 选择项目. */
    private List<CronOption> cronOptions = Lists.newArrayList();

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /*
     * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the cron expression.
     *
     * @return the cron expression
     */
    public String getCronExpression() {
        return cronExpression;
    }

    /**
     * Sets the cron expression.
     *
     * @param cronExpression
     *            the new cron expression
     */
    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    /**
     * Gets the cron options.
     *
     * @return the cron options
     */
    public List<CronOption> getCronOptions() {
        return cronOptions;
    }

    /**
     * Sets the cron options.
     *
     * @param cronOptions
     *            the new cron options
     */
    public void setCronOptions(List<CronOption> cronOptions) {
        this.cronOptions = cronOptions;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
