package com.zeyu.framework.tools.cron.entity;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 具体【秒、分、时、日、月、周、年】的选择结果.
 * Created by zeyuphoenix on 16/9/1.
 */
public class CronOption implements CronConst, Serializable {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    /** Cron的类别,默认秒. */
    private int optionType = SECOND;

    /** Cron的类型,默认每个间隔都触发. */
    private int optionCronType = EVERY;

    /**
     * 防止过多参数，值统一放入其中
     *
     * <pre>
     * NON : 空
     * EVERY : 空
     * RANGE : 第一个代表循环开始时间，第二个代表循环结束时间
     * INCREASE : 第一个代表从那个时间开始，第二个代表间隔
     * DEFINED : 代表具体的选择时间
     * </pre>
     */
    private List<Integer> values = Lists.newArrayList();


    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /*
     * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the option type.
     *
     * @return the option type
     */
    public int getOptionType() {
        return optionType;
    }

    /**
     * Sets the option type.
     *
     * @param optionType
     *            the new option type
     */
    public void setOptionType(int optionType) {
        this.optionType = optionType;
    }

    /**
     * Gets the option cron type.
     *
     * @return the option cron type
     */
    public int getOptionCronType() {
        return optionCronType;
    }

    /**
     * Sets the option cron type.
     *
     * @param optionCronType
     *            the new option cron type
     */
    public void setOptionCronType(int optionCronType) {
        this.optionCronType = optionCronType;
    }

    /**
     * Gets the values.
     *
     * @return the values
     */
    public List<Integer> getValues() {
        return values;
    }

    /**
     * Sets the values.
     *
     * @param values
     *            the new values
     */
    public void setValues(List<Integer> values) {
        this.values = values;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
