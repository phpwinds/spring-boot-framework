package com.zeyu.framework.tools.schedule.annotation;

import com.zeyu.framework.tools.schedule.entity.JobEntity;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 自动配置quartz的job任务
 * Note:
 * <pre>
 * 1. @Target 说明了Annotation所修饰的对象范围：Annotation可被用于 packages、
 *    types（类、接口、枚举、Annotation类型）、类型成员（方法、构造方法、成员变量、枚举值）、
 *    方法参数和本地变量（如循环变量、catch参数）
 * 2. @Retention 定义了该Annotation被保留的时间长短：
 *    a.SOURCE:在源文件中有效（即源文件保留）
 *    b.CLASS:在class文件中有效（即class保留）
 *    c.RUNTIME:在运行时有效（即运行时保留）
 * 3. @Documented用于描述其它类型的annotation应该被作为被标注的程序成员的公共API
 * 4. @Inherited阐述了某个被标注的类型是被继承的
 * </pre>
 * Created by zeyuphoenix on 16/6/21.
 */
@Documented
@Target(ElementType.TYPE)             // 只能在类上
@Retention(RetentionPolicy.RUNTIME)   // 可以被编译和反射
@Inherited
public @interface AutoConfigurationJob {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 任务名称
     */
    String name();

    /**
     * 任务分组
     */
    String group() default JobEntity.NODE_ROOT;

    /**
     * 任务类型
     */
    int type() default 1;

    /**
     * 任务描述
     */
    String description() default "";

    /**
     * 持久化
     */
    boolean persistJobDataAfterExecution() default true;

    /**
     * <pre>
     * 日期型字符串转化为日期 格式
     * {
     * 		"yyyy-MM-dd",
     * 		"yyyy-MM-dd HH:mm:ss",
     * 		"yyyy-MM-dd HH:mm",
     * 		"yyyy/MM/dd",
     * 		"yyyy/MM/dd HH:mm:ss",
     *  	"yyyy/MM/dd HH:mm",
     *  	"yyyy.MM.dd",
     *  	"yyyy.MM.dd HH:mm:ss",
     *  	"yyyy.MM.dd HH:mm"
     * }
     * <pre>
     */
    /**
     * 任务开始时间,不设置即为立即开始
     */
    String startDate() default "";

    /**
     * 任务结束时间,不设置则为永不结束
     */
    String endDate() default "";

    // 任务周期设置: 间隔和cron方式二选一

    /**
     * 任务间隔周期
     */
    int interval() default 10000;

    /* 任务间隔周期单位,最小到秒 */
    TimeUnit unit() default TimeUnit.SECONDS;

    /**
     * 任务调度表达式,如果存在,则interval和unit无效
     */
    String cron() default "";

    /**
     * 传递一些用户自定义数据,任务执行中可以使用
     * 因为注解不能传递复杂对象,请把 Map<String, Object> 转为json字符串进行设置
     */
    String datas() default "";

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
