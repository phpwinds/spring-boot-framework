package com.zeyu.framework.monitors.server.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * 每个网卡的详细信息.
 *
 * @author zeyuphoenix
 */
public class NetInfoStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 网卡名称
    /** The name. */
    private String name;
    // 网卡MAC
    /** The mac. */
    private String mac;
    // 网卡类型
    /** The type. */
    private String type;
    // 网卡IP
    /** The ip. */
    private String ip;
    // 网卡掩码
    /** The netmask. */
    private String netmask;
    // 网卡广播地址
    /** The broadcast. */
    private String broadcast;
    // 网卡活跃点数
    /** The metric. */
    private String metric;
    // 网卡描述
    /** The description. */
    private String description;
    // 接收的总包裹数
    private String rxPackets;
    // 发送的总包裹数
    private String txPackets;
    // 接收到的总字节数
    private String rxBytes;
    // 发送的总字节数
    private String txBytes;
    // 接收到的错误包数
    private String rxErrors;
    // 发送数据包时的错误数
    private String txErrors;
    // 接收时丢弃的包数
    private String rxDropped;
    // 发送时丢弃的包数
    private String txDropped;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the mac.
     *
     * @return the mac
     */
    public String getMac() {
        return mac;
    }

    /**
     * Sets the mac.
     *
     * @param mac
     *            the new mac
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the new ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Gets the netmask.
     *
     * @return the netmask
     */
    public String getNetmask() {
        return netmask;
    }

    /**
     * Sets the netmask.
     *
     * @param netmask
     *            the new netmask
     */
    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    /**
     * Gets the broadcast.
     *
     * @return the broadcast
     */
    public String getBroadcast() {
        return broadcast;
    }

    /**
     * Sets the broadcast.
     *
     * @param broadcast
     *            the new broadcast
     */
    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }

    /**
     * Gets the metric.
     *
     * @return the metric
     */
    public String getMetric() {
        return metric;
    }

    /**
     * Sets the metric.
     *
     * @param metric
     *            the new metric
     */
    public void setMetric(String metric) {
        this.metric = metric;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getRxPackets() {
        return rxPackets;
    }

    public void setRxPackets(String rxPackets) {
        this.rxPackets = rxPackets;
    }

    public String getTxPackets() {
        return txPackets;
    }

    public void setTxPackets(String txPackets) {
        this.txPackets = txPackets;
    }

    public String getRxBytes() {
        return rxBytes;
    }

    public void setRxBytes(String rxBytes) {
        this.rxBytes = rxBytes;
    }

    public String getTxBytes() {
        return txBytes;
    }

    public void setTxBytes(String txBytes) {
        this.txBytes = txBytes;
    }

    public String getRxErrors() {
        return rxErrors;
    }

    public void setRxErrors(String rxErrors) {
        this.rxErrors = rxErrors;
    }

    public String getTxErrors() {
        return txErrors;
    }

    public void setTxErrors(String txErrors) {
        this.txErrors = txErrors;
    }

    public String getRxDropped() {
        return rxDropped;
    }

    public void setRxDropped(String rxDropped) {
        this.rxDropped = rxDropped;
    }

    public String getTxDropped() {
        return txDropped;
    }

    public void setTxDropped(String txDropped) {
        this.txDropped = txDropped;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
