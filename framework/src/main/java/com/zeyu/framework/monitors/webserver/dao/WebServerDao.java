package com.zeyu.framework.monitors.webserver.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.monitors.webserver.entity.WebServerStatus;

/**
 * web server 状态的处理dao
 * Created by zeyuphoenix on 16/8/27.
 */
@MyBatisDao
public interface WebServerDao extends CrudDao<WebServerStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

}
