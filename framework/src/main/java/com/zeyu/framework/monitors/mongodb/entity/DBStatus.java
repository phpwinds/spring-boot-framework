package com.zeyu.framework.monitors.mongodb.entity;

import com.zeyu.framework.core.persistence.entity.SimpleEntity;

/**
 * 每个数据库的详细信息.
 *
 * @author zeyuphoenix
 */
public class DBStatus extends SimpleEntity {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 库名
    private String dbName;
    // 集合数
    private long collections;
    // 记录数
    private long objects;
    // 记录的总大小(B-KB-MB-GB)
    private long dataSize;
    // 索引数
    private long indexes;
    // 事件数
    private long numExtents;
    // 文件大小(B-KB-MB-GB)
    private long fileSize;
    // 数据库状态
    private boolean status;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the db name.
     *
     * @return the db name
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Sets the db name.
     *
     * @param dbName
     *            the new db name
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * Gets the collections.
     *
     * @return the collections
     */
    public long getCollections() {
        return collections;
    }

    /**
     * Sets the collections.
     *
     * @param collections
     *            the new collections
     */
    public void setCollections(long collections) {
        this.collections = collections;
    }

    /**
     * Gets the objects.
     *
     * @return the objects
     */
    public long getObjects() {
        return objects;
    }

    /**
     * Sets the objects.
     *
     * @param objects
     *            the new objects
     */
    public void setObjects(long objects) {
        this.objects = objects;
    }

    /**
     * Gets the data size.
     *
     * @return the data size
     */
    public long getDataSize() {
        return dataSize;
    }

    /**
     * Sets the data size.
     *
     * @param dataSize
     *            the new data size
     */
    public void setDataSize(long dataSize) {
        this.dataSize = dataSize;
    }

    /**
     * Gets the indexes.
     *
     * @return the indexes
     */
    public long getIndexes() {
        return indexes;
    }

    /**
     * Sets the indexes.
     *
     * @param indexes
     *            the new indexes
     */
    public void setIndexes(long indexes) {
        this.indexes = indexes;
    }

    /**
     * Gets the num extents.
     *
     * @return the num extents
     */
    public long getNumExtents() {
        return numExtents;
    }

    /**
     * Sets the num extents.
     *
     * @param numExtents
     *            the new num extents
     */
    public void setNumExtents(long numExtents) {
        this.numExtents = numExtents;
    }

    /**
     * Gets the file size.
     *
     * @return the file size
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * Sets the file size.
     *
     * @param fileSize
     *            the new file size
     */
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * Checks if is status.
     *
     * @return true, if is status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
