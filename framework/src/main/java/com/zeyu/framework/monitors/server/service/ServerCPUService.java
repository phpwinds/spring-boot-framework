package com.zeyu.framework.monitors.server.service;

import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.monitors.server.dao.ServerCPUDao;
import com.zeyu.framework.monitors.server.entity.CPUStatus;
import com.zeyu.framework.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理server cpu监控的service
 * Created by zeyuphoenix on 16/8/27.
 */
@Service
@Transactional(readOnly = true)
public class ServerCPUService extends CrudService<ServerCPUDao, CPUStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Override
    public Page<CPUStatus> findPage(Page<CPUStatus> page, CPUStatus cpuStatus) {

        // 设置默认时间范围，默认当前月
        if (cpuStatus.getBeginDate() == null) {
            cpuStatus.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (cpuStatus.getEndDate() == null) {
            cpuStatus.setEndDate(DateUtils.addMonths(cpuStatus.getBeginDate(), 1));
        }

        return super.findPage(page, cpuStatus);

    }


    /**
     * 查询字段类型列表
     *
     * @return 类型列表
     */
    public List<String> findIndList() {
        return dao.findIndList();
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
