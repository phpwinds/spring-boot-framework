package com.zeyu.framework.monitors.server.entity;

import com.beust.jcommander.internal.Lists;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;
import java.util.List;

/**
 * Server的整体状态监控.
 *
 * @author zeyuphoenix
 */
public class ServerStatus extends SimpleEntity<ServerStatus> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 收集时间
    /** The time. */
    private Date time;

    // Server状态
    /** The status. */
    private boolean status;

    // 持续运行时间(s)
    /** The uptime. */
    private long uptime;

    // 这些项目不会根据时间进行变化，每次取得，不保存数据库，节省数据库空间
    /** CPU相关. */
    // CPU数量.
    private int cpuNum;

    /** Java相关. */
    // Java vm vendor
    private String javaVendor;
    // Java home
    /** The java home. */
    private String javaHome;
    // Java version
    /** The java version. */
    private String javaVersion;

    /** 网络相关. */
    // IP地址.
    private String ip;
    // 网关地址
    /** The gateway. */
    private String gateway;
    // 名称.
    /** The host name. */
    private String hostName;
    // 网卡信息.
    /** The network. */
    private String network;
    // 域名.
    /** The domain name. */
    private String domainName;
    // dns.
    /** The primary dns. */
    private String primaryDns;
    // dns.
    /** The secondary dns. */
    private String secondaryDns;

    /** 系统相关. */
    // 操作系统.
    private String osName;
    // OS description
    /** The description. */
    private String description;
    // OS arch
    /** The arch. */
    private String arch;
    // OS version
    /** The version. */
    private String version;
    // OS vendor
    /** The vendor. */
    private String vendor;
    // OS vendor version
    /** The vendor version. */
    private String vendorVersion;

    // 这些项目需要统计，保存数据库
    /** 内存相关. */
    // 可使用内存.
    private long totalMemory;
    // 剩余内存.
    /** The free memory. */
    private long freeMemory;
    // 已使用的物理内存.
    /** The used memory. */
    private long usedMemory;
    // 内存使用率.
    /** The used memory percent. */
    private double usedMemoryPercent;
    // 总的物理内存.
    /** The actual used memory. */
    private long actualUsedMemory;
    // 剩余的物理内存.
    /** The actual free memory. */
    private long actualFreeMemory;
    // 最大可使用内存.
    /** The max memory. */
    private long maxMemory;
    // 交换区总内存.
    /** The swap total memory. */
    private long swapTotalMemory;
    // 交换区使用内存.
    /** The swap used memory. */
    private long swapUsedMemory;
    // 交换区剩余内存.
    /** The swap free memory. */
    private long swapFreeMemory;

    /** CPU相关. */
    private List<CPUStatus> cpuStatusList = Lists.newArrayList();

    /** JVM相关. */
    // JVM总内存
    private long jvmTotalMemory;
    // JVM最大内存
    /** The jvm max memory. */
    private long jvmMaxMemory;
    // JVM空闲内存
    /** The jvm free memory. */
    private long jvmFreeMemory;

    // 查询参数
    // 开始日期
    private Date beginDate;
    // 结束日期
    private Date endDate;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Checks if is status.
     *
     * @return true, if is status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * Gets the uptime.
     *
     * @return the uptime
     */
    public long getUptime() {
        return uptime;
    }

    /**
     * Sets the uptime.
     *
     * @param uptime
     *            the new uptime
     */
    public void setUptime(long uptime) {
        this.uptime = uptime;
    }

    /**
     * Gets the cpu num.
     *
     * @return the cpu num
     */
    public int getCpuNum() {
        return cpuNum;
    }

    /**
     * Sets the cpu num.
     *
     * @param cpuNum
     *            the new cpu num
     */
    public void setCpuNum(int cpuNum) {
        this.cpuNum = cpuNum;
    }

    /**
     * Gets the java vendor.
     *
     * @return the java vendor
     */
    public String getJavaVendor() {
        return javaVendor;
    }

    /**
     * Sets the java vendor.
     *
     * @param javaVendor
     *            the new java vendor
     */
    public void setJavaVendor(String javaVendor) {
        this.javaVendor = javaVendor;
    }

    /**
     * Gets the java home.
     *
     * @return the java home
     */
    public String getJavaHome() {
        return javaHome;
    }

    /**
     * Sets the java home.
     *
     * @param javaHome
     *            the new java home
     */
    public void setJavaHome(String javaHome) {
        this.javaHome = javaHome;
    }

    /**
     * Gets the java version.
     *
     * @return the java version
     */
    public String getJavaVersion() {
        return javaVersion;
    }

    /**
     * Sets the java version.
     *
     * @param javaVersion
     *            the new java version
     */
    public void setJavaVersion(String javaVersion) {
        this.javaVersion = javaVersion;
    }

    /**
     * Gets the ip.
     *
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets the ip.
     *
     * @param ip
     *            the new ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Gets the gateway.
     *
     * @return the gateway
     */
    public String getGateway() {
        return gateway;
    }

    /**
     * Sets the gateway.
     *
     * @param gateway
     *            the new gateway
     */
    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    /**
     * Gets the host name.
     *
     * @return the host name
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Sets the host name.
     *
     * @param hostName
     *            the new host name
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Gets the network.
     *
     * @return the network
     */
    public String getNetwork() {
        return network;
    }

    /**
     * Sets the network.
     *
     * @param network
     *            the new network
     */
    public void setNetwork(String network) {
        this.network = network;
    }

    /**
     * Gets the domain name.
     *
     * @return the domain name
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Sets the domain name.
     *
     * @param domainName
     *            the new domain name
     */
    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    /**
     * Gets the primary dns.
     *
     * @return the primary dns
     */
    public String getPrimaryDns() {
        return primaryDns;
    }

    /**
     * Sets the primary dns.
     *
     * @param primaryDns
     *            the new primary dns
     */
    public void setPrimaryDns(String primaryDns) {
        this.primaryDns = primaryDns;
    }

    /**
     * Gets the secondary dns.
     *
     * @return the secondary dns
     */
    public String getSecondaryDns() {
        return secondaryDns;
    }

    /**
     * Sets the secondary dns.
     *
     * @param secondaryDns
     *            the new secondary dns
     */
    public void setSecondaryDns(String secondaryDns) {
        this.secondaryDns = secondaryDns;
    }

    /**
     * Gets the os name.
     *
     * @return the os name
     */
    public String getOsName() {
        return osName;
    }

    /**
     * Sets the os name.
     *
     * @param osName
     *            the new os name
     */
    public void setOsName(String osName) {
        this.osName = osName;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the arch.
     *
     * @return the arch
     */
    public String getArch() {
        return arch;
    }

    /**
     * Sets the arch.
     *
     * @param arch
     *            the new arch
     */
    public void setArch(String arch) {
        this.arch = arch;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Gets the vendor.
     *
     * @return the vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Sets the vendor.
     *
     * @param vendor
     *            the new vendor
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * Gets the vendor version.
     *
     * @return the vendor version
     */
    public String getVendorVersion() {
        return vendorVersion;
    }

    /**
     * Sets the vendor version.
     *
     * @param vendorVersion
     *            the new vendor version
     */
    public void setVendorVersion(String vendorVersion) {
        this.vendorVersion = vendorVersion;
    }

    /**
     * Gets the total memory.
     *
     * @return the total memory
     */
    public long getTotalMemory() {
        return totalMemory;
    }

    /**
     * Sets the total memory.
     *
     * @param totalMemory
     *            the new total memory
     */
    public void setTotalMemory(long totalMemory) {
        this.totalMemory = totalMemory;
    }

    /**
     * Gets the free memory.
     *
     * @return the free memory
     */
    public long getFreeMemory() {
        return freeMemory;
    }

    /**
     * Sets the free memory.
     *
     * @param freeMemory
     *            the new free memory
     */
    public void setFreeMemory(long freeMemory) {
        this.freeMemory = freeMemory;
    }

    /**
     * Gets the used memory.
     *
     * @return the used memory
     */
    public long getUsedMemory() {
        return usedMemory;
    }

    /**
     * Sets the used memory.
     *
     * @param usedMemory
     *            the new used memory
     */
    public void setUsedMemory(long usedMemory) {
        this.usedMemory = usedMemory;
    }

    /**
     * Gets the used memory percent.
     *
     * @return the used memory percent
     */
    public double getUsedMemoryPercent() {
        return usedMemoryPercent;
    }

    /**
     * Sets the used memory percent.
     *
     * @param usedMemoryPercent
     *            the new used memory percent
     */
    public void setUsedMemoryPercent(double usedMemoryPercent) {
        this.usedMemoryPercent = usedMemoryPercent;
    }

    /**
     * Gets the actual used memory.
     *
     * @return the actual used memory
     */
    public long getActualUsedMemory() {
        return actualUsedMemory;
    }

    /**
     * Sets the actual used memory.
     *
     * @param actualUsedMemory
     *            the new actual used memory
     */
    public void setActualUsedMemory(long actualUsedMemory) {
        this.actualUsedMemory = actualUsedMemory;
    }

    /**
     * Gets the actual free memory.
     *
     * @return the actual free memory
     */
    public long getActualFreeMemory() {
        return actualFreeMemory;
    }

    /**
     * Sets the actual free memory.
     *
     * @param actualFreeMemory
     *            the new actual free memory
     */
    public void setActualFreeMemory(long actualFreeMemory) {
        this.actualFreeMemory = actualFreeMemory;
    }

    /**
     * Gets the max memory.
     *
     * @return the max memory
     */
    public long getMaxMemory() {
        return maxMemory;
    }

    /**
     * Sets the max memory.
     *
     * @param maxMemory
     *            the new max memory
     */
    public void setMaxMemory(long maxMemory) {
        this.maxMemory = maxMemory;
    }

    public List<CPUStatus> getCpuStatusList() {
        return cpuStatusList;
    }

    public void setCpuStatusList(List<CPUStatus> cpuStatusList) {
        this.cpuStatusList = cpuStatusList;
    }

    /**
     * Gets the swap total memory.
     *
     * @return the swap total memory
     */
    public long getSwapTotalMemory() {
        return swapTotalMemory;
    }

    /**
     * Sets the swap total memory.
     *
     * @param swapTotalMemory
     *            the new swap total memory
     */
    public void setSwapTotalMemory(long swapTotalMemory) {
        this.swapTotalMemory = swapTotalMemory;
    }

    /**
     * Gets the swap used memory.
     *
     * @return the swap used memory
     */
    public long getSwapUsedMemory() {
        return swapUsedMemory;
    }

    /**
     * Sets the swap used memory.
     *
     * @param swapUsedMemory
     *            the new swap used memory
     */
    public void setSwapUsedMemory(long swapUsedMemory) {
        this.swapUsedMemory = swapUsedMemory;
    }

    /**
     * Gets the swap free memory.
     *
     * @return the swap free memory
     */
    public long getSwapFreeMemory() {
        return swapFreeMemory;
    }

    /**
     * Sets the swap free memory.
     *
     * @param swapFreeMemory
     *            the new swap free memory
     */
    public void setSwapFreeMemory(long swapFreeMemory) {
        this.swapFreeMemory = swapFreeMemory;
    }

    /**
     * Gets the jvm total memory.
     *
     * @return the jvm total memory
     */
    public long getJvmTotalMemory() {
        return jvmTotalMemory;
    }

    /**
     * Sets the jvm total memory.
     *
     * @param jvmTotalMemory
     *            the new jvm total memory
     */
    public void setJvmTotalMemory(long jvmTotalMemory) {
        this.jvmTotalMemory = jvmTotalMemory;
    }

    /**
     * Gets the jvm max memory.
     *
     * @return the jvm max memory
     */
    public long getJvmMaxMemory() {
        return jvmMaxMemory;
    }

    /**
     * Sets the jvm max memory.
     *
     * @param jvmMaxMemory
     *            the new jvm max memory
     */
    public void setJvmMaxMemory(long jvmMaxMemory) {
        this.jvmMaxMemory = jvmMaxMemory;
    }

    /**
     * Gets the jvm free memory.
     *
     * @return the jvm free memory
     */
    public long getJvmFreeMemory() {
        return jvmFreeMemory;
    }

    /**
     * Sets the jvm free memory.
     *
     * @param jvmFreeMemory
     *            the new jvm free memory
     */
    public void setJvmFreeMemory(long jvmFreeMemory) {
        this.jvmFreeMemory = jvmFreeMemory;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
