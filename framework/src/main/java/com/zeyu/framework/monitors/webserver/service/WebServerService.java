package com.zeyu.framework.monitors.webserver.service;

import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.monitors.webserver.dao.WebServerDao;
import com.zeyu.framework.monitors.webserver.entity.WebServerStatus;
import com.zeyu.framework.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理web server监控的service
 * Created by zeyuphoenix on 16/8/27.
 */
@Service
@Transactional(readOnly = true)
public class WebServerService extends CrudService<WebServerDao, WebServerStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Override
    public List<WebServerStatus> findList(WebServerStatus webServerStatus) {

        // 设置默认时间范围，默认当前月
        if (webServerStatus.getBeginDate() == null) {
            webServerStatus.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (webServerStatus.getEndDate() == null) {
            webServerStatus.setEndDate(DateUtils.addMonths(webServerStatus.getBeginDate(), 1));
        }

        return super.findList(webServerStatus);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
