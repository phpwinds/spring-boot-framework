package com.zeyu.framework.monitors.mysql.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.entity.SimpleEntity;

import java.util.Date;

/**
 * Mysql的整体状态监控.
 *
 * @author zeyuphoenix
 */
public class MysqlStatus extends SimpleEntity<MysqlStatus> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // ================================================================
    // Fields
    // ================================================================

    // 收集时间
    private Date time;
    // Mysql状态
    private boolean status;

    // 这些项目不会根据时间进行变化，也没有按时间统计的意义，每次取得，不保存数据库，节省数据库空间
    // 数据库IP
    private String host;
    // 端口
    private int port;
    // 数据库名称
    private String dbName;
    // 数据库版本
    private String version;
    // 主机名称
    private String hostName;
    // 进程Id
    private int pid;
    // 进程名
    private String process;
    // 持续运行时间(s)
    private long uptime;
    // Mysql的路径
    private String baseDir;

    // 当前打开的连接的数量
    private int threadConnections;
    // Mysql最大连接数
    private int maxConnections;

    // 每秒钟SQL语句执行次数
    // Questions / seconds
    private float qps;
    // 每秒钟S事务处理数
    // TPS = (Com_commit + Com_rollback) / seconds
    private float tps;

    // 缓存命中率
    // key_buffer_read_hits = (1-key_reads / key_read_requests) * 100%
    // key_buffer_write_hits = (1-key_writes / key_write_requests) * 100%
    private float keyHits;
    /** The key read hits. */
    private float keyReadHits;
    /** The key write hits. */
    private float keyWriteHits;

    // MySQL实例平均每秒钟的输入、输出流量，单位为KB
    // Bytes_sent
    private float sendBytes;
    // Bytes_received
    private float receiveBytes;

    // 查询参数
    // 开始日期
    private Date beginDate;
    // 结束日期
    private Date endDate;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    /**
     * Gets the host.
     *
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets the host.
     *
     * @param host
     *            the new host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Gets the port.
     *
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets the port.
     *
     * @param port
     *            the new port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * Sets the time.
     *
     * @param time
     *            the new time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * Gets the db name.
     *
     * @return the db name
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Sets the db name.
     *
     * @param dbName
     *            the new db name
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * Checks if is status.
     *
     * @return true, if is status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Gets the host name.
     *
     * @return the host name
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Sets the host name.
     *
     * @param hostName
     *            the new host name
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Gets the pid.
     *
     * @return the pid
     */
    public int getPid() {
        return pid;
    }

    /**
     * Sets the pid.
     *
     * @param pid
     *            the new pid
     */
    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     * Gets the process.
     *
     * @return the process
     */
    public String getProcess() {
        return process;
    }

    /**
     * Sets the process.
     *
     * @param process
     *            the new process
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * Gets the uptime.
     *
     * @return the uptime
     */
    public long getUptime() {
        return uptime;
    }

    /**
     * Sets the uptime.
     *
     * @param uptime
     *            the new uptime
     */
    public void setUptime(long uptime) {
        this.uptime = uptime;
    }

    /**
     * Gets the base dir.
     *
     * @return the base dir
     */
    public String getBaseDir() {
        return baseDir;
    }

    /**
     * Sets the base dir.
     *
     * @param baseDir
     *            the new base dir
     */
    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }

    /**
     * Gets the thread connections.
     *
     * @return the thread connections
     */
    public int getThreadConnections() {
        return threadConnections;
    }

    /**
     * Sets the thread connections.
     *
     * @param threadConnections
     *            the new thread connections
     */
    public void setThreadConnections(int threadConnections) {
        this.threadConnections = threadConnections;
    }

    /**
     * Gets the max connections.
     *
     * @return the max connections
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * Sets the max connections.
     *
     * @param maxConnections
     *            the new max connections
     */
    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    /**
     * Gets the qps.
     *
     * @return the qps
     */
    public float getQps() {
        return qps;
    }

    /**
     * Sets the qps.
     *
     * @param qps
     *            the new qps
     */
    public void setQps(float qps) {
        this.qps = qps;
    }

    /**
     * Gets the tps.
     *
     * @return the tps
     */
    public float getTps() {
        return tps;
    }

    /**
     * Sets the tps.
     *
     * @param tps
     *            the new tps
     */
    public void setTps(float tps) {
        this.tps = tps;
    }

    /**
     * Gets the key hits.
     *
     * @return the key hits
     */
    public float getKeyHits() {
        return keyHits;
    }

    /**
     * Sets the key hits.
     *
     * @param keyHits
     *            the new key hits
     */
    public void setKeyHits(float keyHits) {
        this.keyHits = keyHits;
    }

    /**
     * Gets the key read hits.
     *
     * @return the key read hits
     */
    public float getKeyReadHits() {
        return keyReadHits;
    }

    /**
     * Sets the key read hits.
     *
     * @param keyReadHits
     *            the new key read hits
     */
    public void setKeyReadHits(float keyReadHits) {
        this.keyReadHits = keyReadHits;
    }

    /**
     * Gets the key write hits.
     *
     * @return the key write hits
     */
    public float getKeyWriteHits() {
        return keyWriteHits;
    }

    /**
     * Sets the key write hits.
     *
     * @param keyWriteHits
     *            the new key write hits
     */
    public void setKeyWriteHits(float keyWriteHits) {
        this.keyWriteHits = keyWriteHits;
    }

    /**
     * Gets the send bytes.
     *
     * @return the send bytes
     */
    public float getSendBytes() {
        return sendBytes;
    }

    /**
     * Sets the send bytes.
     *
     * @param sendBytes
     *            the new send bytes
     */
    public void setSendBytes(float sendBytes) {
        this.sendBytes = sendBytes;
    }

    /**
     * Gets the receive bytes.
     *
     * @return the receive bytes
     */
    public float getReceiveBytes() {
        return receiveBytes;
    }

    /**
     * Sets the receive bytes.
     *
     * @param receiveBytes
     *            the new receive bytes
     */
    public void setReceiveBytes(float receiveBytes) {
        this.receiveBytes = receiveBytes;
    }


    public Date getBeginDate() {
        return beginDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
