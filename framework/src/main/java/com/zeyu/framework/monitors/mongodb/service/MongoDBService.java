package com.zeyu.framework.monitors.mongodb.service;

import com.zeyu.framework.core.service.CrudService;
import com.zeyu.framework.monitors.mongodb.dao.MongoDBDao;
import com.zeyu.framework.monitors.mongodb.entity.MongoDBStatus;
import com.zeyu.framework.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理mongodb监控的service
 * Created by zeyuphoenix on 16/8/27.
 */
@Service
@Transactional(readOnly = true)
public class MongoDBService extends CrudService<MongoDBDao, MongoDBStatus> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Override
    public List<MongoDBStatus> findList(MongoDBStatus mongoDBStatus) {

        // 设置默认时间范围，默认当前月
        if (mongoDBStatus.getBeginDate() == null) {
            mongoDBStatus.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (mongoDBStatus.getEndDate() == null) {
            mongoDBStatus.setEndDate(DateUtils.addMonths(mongoDBStatus.getBeginDate(), 1));
        }

        return super.findList(mongoDBStatus);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
