package com.zeyu.framework.modules.common.openid;

import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.util.OAuthConfig;

import java.util.Arrays;

/**
 * 第三方登录的管理类
 * Created by zeyuphoenix on 2017/1/3.
 */
public class OpenIdManager {

    // ================================================================
    // Constants
    // ================================================================

    /**第三方认证类型*/
    // 微信
    public static final String PROVIDER_WEIXIN = "weixin";
    // QQ
    public static final String PROVIDER_QQ = "qq";
    // 微博
    public static final String PROVIDER_WEIBO = "weibo";
    // Google
    public static final String PROVIDER_GOOGLE = "google";
    // Yahoo
    public static final String PROVIDER_YAHOO = "yahoo";
    // Hotmail
    public static final String PROVIDER_HOTMAIL = "hotmail";
    // Github
    public static final String PROVIDER_GITHUB = "github";
    // OsChina
    public static final String PROVIDER_OSCHINA = "oschina";

    // 认证登录完成后的返回页面 (一些网站必须使用网站,不能使用ip,请注意の 修改hosts)
    //public final static String AFTER_BIND_URL = "http://127.0.0.1:8080/common/openid/afterLogin";
    public final static String AFTER_BIND_URL = "http://www.zeyuphoenix.com:8080/common/openid/afterLogin";

    // cache name
    public final static String SOCIAL_AUTH_CACHE = "socialauth";
    // cache key
    public final static String SOCIAL_AUTH_KEY = "socialauth_id";

    // used to load the configuration for all providers. Load() method is used to upload the configuration.
    private final static SocialAuthConfig config;

    static {
        // 初始化
        config = SocialAuthConfig.getDefault();

        try {
            config.load();
            // 加载扩展的登录provider
            // hotmail and yahoo not need provider,weixin used other method
            for (String provider_name : Arrays.asList(PROVIDER_QQ, PROVIDER_WEIBO,
                    PROVIDER_GOOGLE, PROVIDER_GITHUB, PROVIDER_OSCHINA)) {
                String className = config.getApplicationProperties()
                        .getProperty(provider_name);
                String consumer_key = config.getApplicationProperties()
                        .getProperty(provider_name + ".consumer_key");
                String consumer_secret = config.getApplicationProperties()
                        .getProperty(provider_name + ".consumer_secret");
                String custom_permissions = config.getApplicationProperties()
                        .getProperty(provider_name + ".custom_permissions");
                OAuthConfig c = new OAuthConfig(consumer_key, consumer_secret);
                if (custom_permissions != null)
                    c.setCustomPermissions(custom_permissions);
                c.setProviderImplClass(Class.forName(className));
                config.addProviderConfig(provider_name, c);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取openid的配置信息
     */
    public static SocialAuthConfig getConfig() {
        return config;
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
