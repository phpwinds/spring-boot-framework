package com.zeyu.framework.modules.sys.dao;

import com.zeyu.framework.core.persistence.annotation.MyBatisDao;
import com.zeyu.framework.core.persistence.dao.CrudDao;
import com.zeyu.framework.modules.sys.entity.Role;

/**
 * 角色DAO接口
 */
@MyBatisDao
public interface RoleDao extends CrudDao<Role> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 根据name获取角色
     *
     * @param role 角色信息
     * @return 角色
     */
    Role getByName(Role role);

    /**
     * 根据ename获取角色
     *
     * @param role 角色信息
     * @return 角色
     */
    Role getByEnname(Role role);

    /**
     * 维护角色与菜单权限关系
     *
     * @param role 角色信息
     * @return 删除结果
     */
    int deleteRoleMenu(Role role);

    /**
     * 维护角色与菜单权限关系
     *
     * @param role 角色信息
     * @return 删除结果
     */
    int insertRoleMenu(Role role);

}
