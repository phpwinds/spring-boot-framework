package com.zeyu.framework.modules.sys.service;

import com.zeyu.framework.core.service.TreeService;
import com.zeyu.framework.modules.sys.dao.OfficeDao;
import com.zeyu.framework.modules.sys.entity.Office;
import com.zeyu.framework.modules.sys.utils.UserUtils;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 机构Service
 */
@Service
@Transactional(readOnly = true)
public class OfficeService extends TreeService<OfficeDao, Office> {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public List<Office> findList(Office office) {
        office.setParentIds(office.getParentIds() + "%");
        return dao.findByParentIdsLike(office);
    }

    @Transactional
    @Override
    public void save(Office office) {
        super.save(office);
        UserUtils.removeCache(UserUtils.CACHE_OFFICE_LIST);
    }

    @Transactional
    @Override
    public void delete(Office office) {
        super.delete(office);
        UserUtils.removeCache(UserUtils.CACHE_OFFICE_LIST);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 通过缓存获取所有office
     */
    public List<Office> findAll() {
        return UserUtils.getOfficeList();
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
