package com.zeyu.framework.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.zeyu.framework.core.common.mapper.JsonMapper;
import com.zeyu.framework.core.persistence.entity.DataEntity;
import com.zeyu.framework.utils.StringUtils;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;
import java.util.Map;

/**
 * 日志Entity
 */
public class Log extends DataEntity<Log> {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * UID
     */
    private static final long serialVersionUID = 1L;

    // 日志类型（1：接入日志；2：错误日志; 3:操作日志）
    public static final String TYPE_ACCESS = "1";
    public static final String TYPE_EXCEPTION = "2";
    public static final String TYPE_OPERATION = "3";

    // ================================================================
    // Fields
    // ================================================================

    // 日志类型（1：接入日志；2：错误日志; 3:操作日志）
    private String type;
    // 日志标题
    private String title;
    // 操作用户的IP地址
    private String remoteAddr;
    // 操作的URI
    private String requestUri;
    // 操作的方式
    private String method;
    // 操作提交的数据
    private String params;
    // 操作用户代理信息
    private String userAgent;
    // 异常信息
    private String exception;

    // 开始日期
    private Date beginDate;
    // 结束日期
    private Date endDate;

    // ================================================================
    // Constructors
    // ================================================================

    public Log() {
        super();
    }

    public Log(String id) {
        super(id);
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonDeserialize(using = JsonMapper.CustomJsonDateDeserializer.class)
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 设置请求参数
     *
     * @param paramMap 请求参数
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @JsonIgnore
    public void setParamMap(Map paramMap) {
        if (paramMap == null) {
            return;
        }
        StringBuilder params = new StringBuilder();
        for (Map.Entry<String, String[]> param : ((Map<String, String[]>) paramMap).entrySet()) {
            params.append("".equals(params.toString()) ? "" : "&").append(param.getKey()).append("=");
            String paramValue = (param.getValue() != null && param.getValue().length > 0 ? param.getValue()[0] : "");
            params.append(StringUtils.abbr(StringUtils.endsWithIgnoreCase(param.getKey(), "password") ? "" : paramValue, 100));
        }
        this.params = params.toString();
    }

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
