package com.zeyu.framework.modules.sys.service;

import com.zeyu.framework.core.security.interfaces.SecurityService;
import com.zeyu.framework.core.security.interfaces.SecurityUser;
import com.zeyu.framework.core.security.session.SessionDAO;
import com.zeyu.framework.core.security.utils.SecurityUtils;
import com.zeyu.framework.core.service.BaseService;
import com.zeyu.framework.modules.sys.dao.UserDao;
import com.zeyu.framework.modules.sys.entity.Menu;
import com.zeyu.framework.modules.sys.entity.User;
import com.zeyu.framework.modules.sys.utils.UserUtils;
import com.zeyu.framework.utils.StringUtils;

import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * shiro的登录验证实现类
 * Created by zeyuphoenix on 16/8/2.
 */
@Service
@Transactional(readOnly = true)
public class SecurityServiceImpl extends BaseService implements SecurityService {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private UserDao userDao;

    @Autowired
    private SessionDAO sessionDao;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    /**
     * 根据登录名获取用户
     *
     * @param loginName 登录名
     * @return 用户
     */
    @Override
    public SecurityUser getUserByLoginName(String loginName) {
        return UserUtils.getByLoginName(loginName);
    }

    /**
     * @see SecurityService
     */
    @Override
    public SessionDAO getSessionDao() {
        return sessionDao;
    }

    /**
     * @see SecurityService
     */
    @Transactional
    @Override
    public void updateUserLoginInfo(SecurityUser securityUser) {
        if (securityUser != null && securityUser instanceof User) {
            User user = (User) securityUser;

            // 保存上次登录信息
            user.setOldLoginIp(user.getLoginIp());
            user.setOldLoginDate(user.getLoginDate());
            // 更新本次登录信息
            Session session = SecurityUtils.getSession();
            if (session != null) {
                user.setLoginIp(session.getHost());
            } else {
                user.setLoginIp("127.0.0.1");
            }
            user.setLoginDate(new Date());
            userDao.updateLoginInfo(user);
        }
    }

    /**
     * @see SecurityService
     */
    @Override
    public List<String> findAllMenuPermissions() {
        List<Menu> menus = UserUtils.getMenuList();
        return menus.stream().filter(menu -> StringUtils.isNoneBlank(menu.getPermission()))
                .map(Menu::getPermission).collect(Collectors.toList());
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
