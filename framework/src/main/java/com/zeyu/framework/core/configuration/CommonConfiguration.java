package com.zeyu.framework.core.configuration;

import com.zeyu.framework.core.common.condition.ConvertCondition;
import com.zeyu.framework.core.common.condition.LinuxCondition;
import com.zeyu.framework.tools.docconvert.OfficeConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * 公用的配置信息
 * Created by zeyuphoenix on 2017/2/28.
 */
@Configuration  //自动配置
public class CommonConfiguration {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private ApplicationContext applicationContext;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 注册office转换服务
     */
    @Bean(name = "officeConvertService", initMethod = "init", destroyMethod = "destroy")
    @Conditional({LinuxCondition.class, ConvertCondition.class})
    @ConditionalOnClass({OfficeConvertService.class})
    public OfficeConvertService officeConvertService() {
        String officePort = applicationContext.getEnvironment().getProperty("framework.convert.office.port");
        String officeHome = applicationContext.getEnvironment().getProperty("framework.convert.office.home");
        return new OfficeConvertService(officePort, officeHome);
    }

    // ================================================================
    // Getter & Setter‰
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
