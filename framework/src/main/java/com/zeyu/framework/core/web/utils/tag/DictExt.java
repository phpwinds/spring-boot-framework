package com.zeyu.framework.core.web.utils.tag;

import com.zeyu.framework.modules.sys.utils.DictUtils;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * beetl 使用自定义函数写了dict tag功能，你可以像使用jsp的标签那样使用
 * <!--:
 * if(dict.getDictList('type')) {}
 * -->
 * Created by zeyuphoenix on 2016/3/23.
 */
@Service
public class DictExt {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取字典标签
     *
     * @return 字典显示
     */
    public static String getDictLabel(String value, String type, String defaultValue) {
        return DictUtils.getDictLabel(value, type, defaultValue);
    }

    /**
     * 获取字典标签(多个)
     *
     * @return 字典显示列表
     */
    public static String getDictLabels(String values, String type, String defaultValue) {
        return DictUtils.getDictLabels(values, type, defaultValue);
    }

    /**
     * 获取字典值
     *
     * @return 字典值
     */
    public static String getDictValue(String label, String type, String defaultValue) {
        return DictUtils.getDictValue(label, type, defaultValue);
    }

    /**
     * 获取字典对象列表
     *
     * @return 字典对象列表
     */
    public static List<?> getDictList(String type) {
        return DictUtils.getDictList(type);
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
