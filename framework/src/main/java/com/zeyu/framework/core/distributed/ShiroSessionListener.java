package com.zeyu.framework.core.distributed;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 用户退出后，Session没有从Redis中销毁，虽然当前重新new了一个，但会对统计带来干扰，通过SessionListener解决
 * Created by zeyuphoenix on 16/7/6.
 */
public class ShiroSessionListener implements SessionListener {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ShiroSessionListener.class);

    // ================================================================
    // Fields
    // ================================================================

    @Autowired
    private RedisSessionDAO redisSessionDAO;

    @Autowired
    private ShiroSessionService shiroSessionService;

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public void onStart(Session session) {
        // 会话创建时触发
        if (logger.isDebugEnabled()) {
            logger.debug("ShiroSessionListener session {} is start", session.getId());
        }
    }

    @Override
    public void onStop(Session session) {
        // 会话销毁时触发
        redisSessionDAO.delete(session);
        // 通知其他分布式的server的ehcache进行session销毁
        shiroSessionService.sendUncacheSessionMessage(session.getId());
        if (logger.isDebugEnabled()) {
            logger.debug("ShiroSessionListener session {} is stop", session.getId());
        }
    }

    @Override
    public void onExpiration(Session session) {
        // 会话过期时触发
        redisSessionDAO.delete(session);
        // 通知其他分布式的server的ehcache进行session销毁
        shiroSessionService.sendUncacheSessionMessage(session.getId());
        if (logger.isDebugEnabled()) {
            logger.debug("ShiroSessionListener session {} is expiration", session.getId());
        }
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
