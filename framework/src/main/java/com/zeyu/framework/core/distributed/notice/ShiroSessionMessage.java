package com.zeyu.framework.core.distributed.notice;

import com.zeyu.framework.utils.SerializeUtils;
import org.springframework.data.redis.connection.DefaultMessage;

import java.io.Serializable;

/**
 * shiro session 变更后,通知redis
 * Created by zeyuphoenix on 16/7/6.
 */
public class ShiroSessionMessage extends DefaultMessage {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // message主体
    public final MessageBody msgBody;

    // ================================================================
    // Constructors
    // ================================================================

    public ShiroSessionMessage(byte[] channel, byte[] body) {
        super(channel, body);
        msgBody = SerializeUtils.deserializeFromString(new String(body));
    }

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    /**
     * 合入了shiro session的message
     */
    public static class MessageBody implements Serializable {

        // ================================================================
        // Fields
        // ================================================================

        /**
         * message 内容
         */
        public final Serializable sessionId;
        public final String nodeId;
        private String msg = "";

        // ================================================================
        // Constructors
        // ================================================================

        public MessageBody(Serializable sessionId, String nodeId) {
            this.sessionId = sessionId;
            this.nodeId = nodeId;
        }

        public MessageBody(Serializable sessionId, String nodeId, String msg) {
            this.sessionId = sessionId;
            this.nodeId = nodeId;
            this.msg = msg;
        }

        // ================================================================
        // Methods from/for super Interfaces or SuperClass
        // ================================================================

        @Override
        public String toString() {
            return "MessageBody{" +
                    "sessionId='" + sessionId + '\'' +
                    ", nodeId='" + nodeId + '\'' +
                    ", msg='" + msg + '\'' +
                    '}';
        }
    }

    // ================================================================
    // Test Methods
    // ================================================================

}
