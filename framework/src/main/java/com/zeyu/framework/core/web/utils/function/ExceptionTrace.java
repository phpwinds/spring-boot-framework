package com.zeyu.framework.core.web.utils.function;

import com.zeyu.framework.utils.Exceptions;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.beetl.ext.web.WebVariable;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 异常处理
 * Created by zeyuphoenix on 2016/3/30.
 */
public class ExceptionTrace implements Function {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public Object call(Object[] paras, Context ctx) {
        // 参数为空
        if (paras == null || paras.length == 0) {
            return "";
        }

        HttpServletRequest request = (HttpServletRequest) ctx.getGlobal("request");
        WebVariable servlet = (WebVariable) ctx.getGlobal("servlet");
        HttpServletResponse response = servlet.getResponse();

        response.setStatus(500);
        // 获取异常类
        Throwable ex = Exceptions.getThrowable(request);
        if (ex != null) {
            LoggerFactory.getLogger("500.html").error(ex.getMessage(), ex);
        }

        // 编译错误信息
        StringBuilder sb = new StringBuilder("错误信息：\n");
        if (ex != null) {
            sb.append(Exceptions.getStackTraceAsString(ex));
        } else {
            sb.append("未知错误.\n\n");
        }

        // 如果是异步请求或是手机端，则直接返回信息
        if (paras[0] != null && Boolean.valueOf(paras[0].toString())) {
            try {
                ctx.byteWriter.write(sb.toString().getBytes("UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================
}
