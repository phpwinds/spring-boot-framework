package com.zeyu.framework.core.common.condition;

import com.zeyu.framework.core.common.Constant;
import com.zeyu.framework.monitors.server.utils.OsCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 某些服务必须在linux才能启动,判断是否可启动
 * Created by zeyuphoenix on 16/7/19.
 */
public class LinuxCondition implements Condition, Constant {

    // ================================================================
    // Constants
    // ================================================================

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(LinuxCondition.class);

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {

        if (OsCheck.getOperatingSystemType() == OsCheck.OSType.Linux) {
            logger.debug("software is run on linux system");
            return true;
        }

        return false;
    }

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
