package com.zeyu.framework.core.security.session;

import com.zeyu.framework.core.common.Constant;
import com.zeyu.framework.core.web.servlet.Servlets;
import com.zeyu.framework.utils.StringUtils;

import org.apache.shiro.session.Session;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * shiro session dao
 */
public interface SessionDAO extends org.apache.shiro.session.mgt.eis.SessionDAO, Constant {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    /**
     * 获取活动会话
     *
     * @param includeLeave 是否包括离线（最后访问时间大于3分钟为离线会话）
     * @return 会话列表
     */
    Collection<Session> getActiveSessions(boolean includeLeave);

    /**
     * 获取活动会话
     *
     * @param includeLeave  是否包括离线（最后访问时间大于3分钟为离线会话）
     * @param principal     根据登录者对象获取活动会话
     * @param filterSession 不为空，则过滤掉（不包含）这个会话。
     * @return 会话列表
     */
    Collection<Session> getActiveSessions(boolean includeLeave, Object principal, Session filterSession);


    /**
     * 根据客户端请求获取session
     *
     * @param request   客户端请求
     * @param sessionId 会话id
     */
    default Map<String, Object> getSession(HttpServletRequest request, Serializable sessionId) {

        Map<String, Object> vals = new HashMap<>();
        vals.put("static", false);
        if (request != null) {
            String uri = request.getServletPath();
            // 如果是静态文件，则不获取SESSION
            if (Servlets.isStaticFile(uri)) {
                vals.put("static", true);
                return vals;
            }
            Session s = (Session) request.getAttribute("session_" + sessionId);
            vals.put("session", s);
        }

        return vals;
    }

    /**
     * 更加客户端请求判断是否需要更新session
     *
     * @param request 客户端请求
     * @return 是否需要更新session
     */
    default boolean needUpdate(HttpServletRequest request) {

        if (request != null) {
            String uri = request.getServletPath();
            // 如果是静态文件，则不更新SESSION
            if (Servlets.isStaticFile(uri)) {
                return false;
            }
            // 如果是视图文件，则不更新SESSION
            if (StringUtils.startsWith(uri, WEB_VIEW_PREFIX) && StringUtils.endsWith(uri, WEB_VIEW_SUFFIX)) {
                return false;
            }
            // 手动控制不更新SESSION
            String updateSession = request.getParameter("updateSession");
            if (FALSE.equals(updateSession) || NO.equals(updateSession)) {
                return false;
            }
        }
        return true;
    }
}
