## 转换库改为手动配置
由于 `phantomjs` 的库越来越大,linux更是达到了惊人的65m，如果按照以前的做法，把这几个包都打入jar内，再通过读取系统类型，选择不同类库，虽然可以实现，但是会导致jar包增大130兆左右，实在是太大，不合理
    1. 现在首先需要根据系统把 `phantomjs` 放在可访问的目录
        * [ ] 可以通过 `google` 搜索下载 `phantomjs`
        * [v] 可以使用工程的 `tools` 目录下的library，选择对应系统的版本
    2. 配置 application.properties 的framework.convert.phantomjs路径
    3. 查看 `app-convert.properties` 的端口是否占用，默认7777
        `netstat -ant | grep 7777`
    
   


