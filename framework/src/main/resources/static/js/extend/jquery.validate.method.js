/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese, 中文 (Zhōngwén), 汉语, 漢語)
 */
(function ($) {
    $.extend($.validator.messages, {
        required: "必填信息",
        remote: "请修正该信息",
        email: "请输入正确格式的电子邮件",
        url: "请输入合法的网址",
        date: "请输入合法的日期",
        dateISO: "请输入合法的日期 (ISO).",
        number: "请输入合法的数字",
        digits: "只能输入整数",
        creditcard: "请输入合法的信用卡号",
        equalTo: "请再次输入相同的值",
        accept: "请输入拥有合法后缀名的字符串",
        maxlength: $.validator.format("请输入一个长度最多是 {0} 的字符串"),
        minlength: $.validator.format("请输入一个长度最少是 {0} 的字符串"),
        rangelength: $.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符串"),
        range: $.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
        max: $.validator.format("请输入一个最大为 {0} 的值"),
        min: $.validator.format("请输入一个最小为 {0} 的值")
    });
}(jQuery));

jQuery.validator.addMethod("ip", function (value, element) {
    return this.optional(element) || (/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/.test(value) && (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256));
}, "请输入合法的IP地址");

jQuery.validator.addMethod("abc", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9_]*$/.test(value);
}, "请输入字母数字或下划线");

jQuery.validator.addMethod("username", function (value, element) {
    return this.optional(element) || /^[a-zA-Z0-9][a-zA-Z0-9_]{2,19}$/.test(value);
}, "3-20位字母或数字开头，允许字母数字下划线");

jQuery.validator.addMethod("noEqualTo", function (value, element, param) {
    return value != $(param).val();
}, "请再次输入不同的值");

jQuery.validator.addMethod("gt", function (value, element, param) {
    return value > $(param).val();
}, "请输入更大的值");

jQuery.validator.addMethod("lt", function (value, element, param) {
    return value < $(param).val();
}, "请输入更小的值");

//真实姓名验证
jQuery.validator.addMethod("realName", function (value, element) {
    return this.optional(element) || /^[\u4e00-\u9fa5]{2,30}$/.test(value);
}, "姓名只能为2-30个汉字");

// 字符验证
jQuery.validator.addMethod("userName", function (value, element) {
    return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
}, "登录名只能包括中文字、英文字母、数字和下划线");

// 手机号码验证
jQuery.validator.addMethod("mobile", function (value, element) {
    var length = value.length;
    return this.optional(element) || (length == 11 && /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(value));
}, "请正确填写您的手机号码");

// 电话号码验证
jQuery.validator.addMethod("simplePhone", function (value, element) {
    var tel = /^(\d{3,4}-?)?\d{7,9}$/g;
    return this.optional(element) || (tel.test(value));
}, "请正确填写您的电话号码");

// 电话号码验证     
jQuery.validator.addMethod("phone", function (value, element) {
    var tel = /(^0[1-9]{1}\d{9,10}$)|(^1[3,5,7,8]\d{9}$)/g;
    return this.optional(element) || (tel.test(value));
}, "格式为:固话为区号(3-4位)号码(7-9位),手机为:13,15,17,18号段");

// 邮政编码验证
jQuery.validator.addMethod("zipCode", function (value, element) {
    var tel = /^[0-9]{6}$/;
    return this.optional(element) || (tel.test(value));
}, "请正确填写您的邮政编码");

//QQ号码验证
jQuery.validator.addMethod("qq", function (value, element) {
    var tel = /^[1-9][0-9]{4,}$/;
    return this.optional(element) || (tel.test(value));
}, "请正确填写您的QQ号码");

//校验身份证号
jQuery.validator.addMethod("card", function (value, element) {
    return this.optional(element) || checkIdcard(value);
}, "请输入正确的身份证号码(15-18位)")