/**
 * cron
 * Created by zeyuphoenix on 2016/2/13.
 */
$(function () {

    // 反解析到UI
    $('#btnFan').on('click', function () {
        btnFan();
    });

    // 每时刻触发
    $('.everyTime').on('click', function () {
        var item = $("input[name=v_" + $(this).attr('name') + "]");
        item.val("*");
        item.change();
    });

    // 循环触发
    $('.cycle').on('click', function () {
        var name = $(this).attr('name');
        var ns = $(this).parents('.row:first').find(".spinner");
        var start = ns.eq(0).val();
        var end = ns.eq(1).val();
        var item = $("input[name=v_" + name + "]");
        item.val(start + "-" + end);
        item.change();
    });

    // 递增触发
    $('.startOn').on('click', function () {
        var name = $(this).attr('name');
        var ns = $(this).parents('.row:first').find(".spinner");
        var start = ns.eq(0).val();
        var end = ns.eq(1).val();
        var item = $("input[name=v_" + name + "]");
        item.val(start + "/" + end);
        item.change();
    });

    // 未指定触发
    $('.unAppoint').on('click', function () {
        var name = $(this).attr('name');
        var val = "?";
        if (name == "year")
            val = "";
        var item = $("input[name=v_" + name + "]");
        item.val(val);
        item.change();
    });

    // 本月最后一天触发
    $('.lastDay').on('click', function () {
        var name = $(this).attr('name');
        var item = $("input[name=v_" + name + "]");
        item.val("L");
        item.change();
    });

    // 序号触发
    $('.weekOfDay').on('click', function () {
        var name = $(this).attr('name');
        var ns = $(this).parents('.row:first').find(".spinner");
        var start = ns.eq(0).val();
        var end = ns.eq(1).val();
        var item = $("input[name=v_" + name + "]");
        item.val(start + "#" + end);
        item.change();
    });

    // 最后触发
    $('.lastWeek').on('click', function () {
        var name = $(this).attr('name');
        var item = $("input[name=v_" + name + "]");
        var ns = $(this).parents('.row:first').find(".spinner");
        var start = ns.eq(0).val();
        item.val(start + "L");
        item.change();
    });

    // 最近工作日触发
    $('.workDay').on('click', function () {
        var name = $(this).attr('name');
        var ns = $(this).parents('.row:first').find(".spinner");
        var start = ns.eq(0).val();
        var item = $("input[name=v_" + name + "]");
        item.val(start + "W");
        item.change();
    });

    // spinner
    $(".spinner").on("spin", function (event, ui) {
        var ns = $(this).parents('.row:first').find("input[type=radio]");
        ns.click();
        ns.prop('checked', 'checked');
    });

    // cron表达式
    var vals = $("input[name^='v_']");
    var cron = $("#cron");
    // 表达式事件
    vals.change(function () {
        var item = [];
        vals.each(function () {
            item.push(this.value);
        });
        //修复表达式错误BUG，如果后一项不为* 那么前一项肯定不为为*，要不然就成了每秒执行了
        //获取当前选中tab
        var currentIndex = 0;
        $(".nav-tabs>li").each(function (i, item) {
            if ($(item).hasClass("active")) {
                currentIndex = i;
                return false;
            }
        });
        //当前选中项之前的如果为*，则都设置成0
        for (var i = currentIndex; i >= 1; i--) {
            if (item[i] != "*" && item[i - 1] == "*") {
                //item[i - 1] = "0";
            }
        }
        //当前选中项之后的如果不为*则都设置成*
        if (item[currentIndex] == "*") {
            for (var i = currentIndex + 1; i < item.length; i++) {
                if (i == 5) {
                    item[i] = "?";
                } else {
                    item[i] = "*";
                }
            }
        }
        cron.val(item.join(" ")).change();
    });

    // 秒
    var secondList = $(".secondList").find('input[type=checkbox]');
    secondList.change(function () {
        var sencond_appoint = $(".secondList").find('input[type=radio]').prop("checked");
        if (sencond_appoint) {
            var vals = [];
            secondList.each(function () {
                if (this.checked) {
                    vals.push(this.value);
                }
            });
            var val = "?";
            if (vals.length > 0 && vals.length < 59) {
                val = vals.join(",");
            } else if (vals.length == 59) {
                val = "*";
            }
            var item = $("input[name=v_second]");
            item.val(val);
            item.change();
        }
    });

    // 分
    var minList = $(".minList").find('input[type=checkbox]');
    minList.change(function () {
        var min_appoint = $(".minList").find('input[type=radio]').prop("checked");
        if (min_appoint) {
            var vals = [];
            minList.each(function () {
                if (this.checked) {
                    vals.push(this.value);
                }
            });
            var val = "?";
            if (vals.length > 0 && vals.length < 59) {
                val = vals.join(",");
            } else if (vals.length == 59) {
                val = "*";
            }
            var item = $("input[name=v_min]");
            item.val(val);
            item.change();
        }
    });

    // 小时
    var hourList = $(".hourList").find('input[type=checkbox]');
    hourList.change(function () {
        var hour_appoint = $(".hourList").find('input[type=radio]').prop("checked");
        if (hour_appoint) {
            var vals = [];
            hourList.each(function () {
                if (this.checked) {
                    vals.push(this.value);
                }
            });
            var val = "?";
            if (vals.length > 0 && vals.length < 24) {
                val = vals.join(",");
            } else if (vals.length == 24) {
                val = "*";
            }
            var item = $("input[name=v_hour]");
            item.val(val);
            item.change();
        }
    });

    // 天
    var dayList = $(".dayList").find('input[type=checkbox]');
    dayList.change(function () {
        var day_appoint = $(".dayList").find('input[type=radio]').prop("checked");
        if (day_appoint) {
            var vals = [];
            dayList.each(function () {
                if (this.checked) {
                    vals.push(this.value);
                }
            });
            var val = "?";
            if (vals.length > 0 && vals.length < 31) {
                val = vals.join(",");
            } else if (vals.length == 31) {
                val = "*";
            }
            var item = $("input[name=v_day]");
            item.val(val);
            item.change();
        }
    });

    // 月
    var monthList = $(".monthList").find('input[type=checkbox]');
    monthList.change(function () {
        var month_appoint = $(".monthList").find('input[type=radio]').prop("checked");
        if (month_appoint) {
            var vals = [];
            monthList.each(function () {
                if (this.checked) {
                    vals.push(this.value);
                }
            });
            var val = "?";
            if (vals.length > 0 && vals.length < 12) {
                val = vals.join(",");
            } else if (vals.length == 12) {
                val = "*";
            }
            var item = $("input[name=v_month]");
            item.val(val);
            item.change();
        }
    });

    // 周
    var weekList = $(".weekList").find('input[type=checkbox]');
    weekList.change(function () {
        var week_appoint = $(".weekList").find('input[type=radio]').prop("checked");
        if (week_appoint) {
            var vals = [];
            weekList.each(function () {
                if (this.checked) {
                    vals.push(this.value);
                }
            });
            var val = "?";
            if (vals.length > 0 && vals.length < 7) {
                val = vals.join(",");
            } else if (vals.length == 7) {
                val = "*";
            }
            var item = $("input[name=v_week]");
            item.val(val);
            item.change();
        }
    });

    // 指定时刻触发
    $('.appoint').on('click', function () {
        var name = $(this).attr('name');
        if (this.checked) {
            if (name == 'second') {
                // 秒
                if ($(secondList).filter(":checked").length == 0) {
                    $(secondList.eq(0)).attr("checked", true);
                }
                secondList.eq(0).change();
            } else if (name == 'min') {
                // 分
                if ($(minList).filter(":checked").length == 0) {
                    $(minList.eq(0)).attr("checked", true);
                }
                minList.eq(0).change();
            } else if (name == 'hour') {
                // 小时
                if ($(hourList).filter(":checked").length == 0) {
                    $(hourList.eq(0)).attr("checked", true);
                }
                hourList.eq(0).change();
            } else if (name == 'day') {
                // 天
                if ($(dayList).filter(":checked").length == 0) {
                    $(dayList.eq(0)).attr("checked", true);
                }
                dayList.eq(0).change();
            } else if (name == 'month') {
                // 月
                if ($(monthList).filter(":checked").length == 0) {
                    $(monthList.eq(0)).attr("checked", true);
                }
                monthList.eq(0).change();
            } else if (name == 'week') {
                // 周
                if ($(weekList).filter(":checked").length == 0) {
                    $(weekList.eq(0)).attr("checked", true);
                }
                weekList.eq(0).change();
            }
        }
    });

    // 反解析
    function btnFan() {
        //获取参数中表达式的值
        var txt = $("#cron").val();
        if (txt && txt != '') {
            var regs = txt.split(' ');
            $("input[name=v_second]").val(regs[0]);
            $("input[name=v_min]").val(regs[1]);
            $("input[name=v_hour]").val(regs[2]);
            $("input[name=v_day]").val(regs[3]);
            $("input[name=v_month]").val(regs[4]);
            $("input[name=v_week]").val(regs[5]);

            initObj(regs[0], "second");
            initObj(regs[1], "min");
            initObj(regs[2], "hour");
            initDay(regs[3]);
            initMonth(regs[4]);
            initWeek(regs[5]);

            if (regs.length > 6) {
                $("input[name=v_year]").val(regs[6]);
                initYear(regs[6]);
            }
        }
    }

    // 首字母大写
    function upper(str) {
        return str.replace(/(\w)/, function (v) {
            return v.toUpperCase()
        });
    }

    /**
     * 初始化秒、分、小时
     */
    function initObj(strVal, strid) {
        var ary = null;
        var radios = $("input[name='" + strid + "'");
        if (strVal == "*") {
            radios.eq(0).prop("checked", "checked");
        } else if (strVal.split('-').length > 1) {
            ary = strVal.split('-');
            radios.eq(1).prop("checked", "checked");
            $("#cycle" + upper(strid) + "Start").spinner("value", ary[0]);
            $("#cycle" + upper(strid) + "End").spinner("value", ary[1]);
        } else if (strVal.split('/').length > 1) {
            ary = strVal.split('/');
            radios.eq(2).prop("checked", "checked");
            $("#incr" + upper(strid) + "Start").spinner("value", ary[0]);
            $("#incr" + upper(strid) + "End").spinner("value", ary[1]);
        } else {
            radios.eq(3).prop("checked", "checked");
            $("." + strid + "List input[type='checkbox']").prop("checked", false);
            if (strVal != "?") {
                ary = strVal.split(",");
                for (var i = 0; i < ary.length; i++) {
                    $("." + strid + "List input[value='" + ary[i] + "']").prop("checked", "checked");
                }
            }
        }
    }

    /**
     * 初始化天
     */
    function initDay(strVal) {
        var ary = null;
        var dayRadios = $("input[name='day']");
        if (strVal == "*") {
            dayRadios.eq(0).prop("checked", "checked");
        } else if (strVal == "?") {
            dayRadios.eq(1).prop("checked", "checked");
        } else if (strVal.split('-').length > 1) {
            ary = strVal.split('-');
            dayRadios.eq(2).prop("checked", "checked");
            $("#cycleDayStart").spinner('value', ary[0]);
            $("#cycleDayEnd").spinner('value', ary[1]);
        } else if (strVal.split('/').length > 1) {
            ary = strVal.split('/');
            dayRadios.eq(3).prop("checked", "checked");
            $("#incrDayStart").spinner('value', ary[0]);
            $("#incrDayEnd").spinner('value', ary[1]);
        } else if (strVal.split('W').length > 1) {
            ary = strVal.split('W');
            dayRadios.eq(4).prop("checked", "checked");
            $("#workdaySpinner").spinner('value', ary[0]);
        } else if (strVal == "L") {
            dayRadios.eq(5).prop("checked", "checked");
        } else {
            dayRadios.eq(6).prop("checked", "checked");
            $(".dayList input[type='checkbox']").prop("checked", false);
            ary = strVal.split(",");
            for (var i = 0; i < ary.length; i++) {
                $(".dayList input[value='" + ary[i] + "']").prop("checked", "checked");
            }
        }
    }

    /**
     * 初始化月
     */
    function initMonth(strVal) {
        var ary = null;
        var monthRadios = $("input[name='month']");
        if (strVal == "*") {
            monthRadios.eq(0).prop("checked", "checked");
        } else if (strVal == "?") {
            monthRadios.eq(1).prop("checked", "checked");
        } else if (strVal.split('-').length > 1) {
            ary = strVal.split('-');
            monthRadios.eq(2).prop("checked", "checked");
            $("#cycleMonthStart").spinner("value", ary[0]);
            $("#cycleMonthEnd").spinner("value", ary[1]);
        } else if (strVal.split('/').length > 1) {
            ary = strVal.split('/');
            monthRadios.eq(3).prop("checked", "checked");
            $("#incrMonthStart").spinner("value", ary[0]);
            $("#incrMonthEnd").spinner("value", ary[1]);

        } else {
            monthRadios.eq(4).prop("checked", "checked");
            $(".monthList input[type='checkbox']").prop("checked", false);
            ary = strVal.split(",");
            for (var i = 0; i < ary.length; i++) {
                $(".monthList input[value='" + ary[i] + "']").prop("checked", "checked");
            }
        }
    }

    /**
     * 初始化周
     */
    function initWeek(strVal) {
        var ary = null;
        var weekRadios = $("input[name='week']");
        if (strVal == "*") {
            weekRadios.eq(0).prop("checked", "checked");
        } else if (strVal == "?") {
            weekRadios.eq(1).prop("checked", "checked");
        } else if (strVal.split('-').length > 1) {
            ary = strVal.split('-');
            weekRadios.eq(2).prop("checked", "checked");
            $("#cycleWeekStart").spinner("value", ary[0]);
            $("#cycleWeekEnd").spinner("value", ary[1]);
        } else if (strVal.split('/').length > 1) {
            ary = strVal.split('/');
            weekRadios.eq(3).prop("checked", "checked");
            $("#incrWeekStart").spinner("value", ary[0]);
            $("#incrWeekEnd").spinner("value", ary[1]);
        } else if (strVal.split('#').length > 1) {
            ary = strVal.split('#');
            weekRadios.eq(4).prop("checked", "checked");
            $("#orderWeekStart").spinner("value", ary[0]);
            $("#orderWeekEnd").spinner("value", ary[1]);
        } else if (strVal.split('L').length > 1) {
            ary = strVal.split('L');
            weekRadios.eq(5).prop("checked", "checked");
            $("#lastWeekSpinner").spinner("value", ary[0]);
        } else {
            weekRadios.eq(6).prop("checked", "checked");
            ary = strVal.split(",");
            $(".weekList input[type='checkbox']").prop("checked", false);
            for (var i = 0; i < ary.length; i++) {
                $(".weekList input[value='" + ary[i] + "']").prop("checked", "checked");
            }
        }
    }

    /**
     * 初始化年
     */
    function initYear(strVal) {
        var ary = null;
        var yearRadios = $("input[name='year']");
        if (strVal == "*") {
            yearRadios.eq(1).prop("checked", "checked");
        } else if (strVal.split('-').length > 1) {
            ary = strVal.split('-');
            yearRadios.eq(2).prop("checked", "checked");
            $("#cycleYearStart").spinner("value", ary[0]);
            $("#cycleYearEnd").spinner("value", ary[1]);
        }
    }
});