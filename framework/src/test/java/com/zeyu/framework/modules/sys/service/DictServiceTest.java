package com.zeyu.framework.modules.sys.service;

import com.zeyu.framework.core.persistence.Page;
import com.zeyu.framework.modules.sys.entity.Dict;
import com.zeyu.framework.modules.sys.entity.User;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;

/**
 * dict service test
 * Created by zeyuphoenix on 16/8/3.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@FixMethodOrder(MethodSorters.DEFAULT)   //按方法顺序执行
public class DictServiceTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(DictServiceTest.class);

    @Autowired
    private DictService dictService;

    @Test
    public void getById() throws Exception {
        Dict dict = dictService.get("10");
        if (dict != null) {
            logger.info("query by id is {}", dict);
        }
    }

    @Test
    public void save() throws Exception {
        Dict dict = new Dict();
        dict.setType("test");
        dict.setId("test-12345");
        dict.setFixed(0);
        dict.setSort(10);
        dict.setDescription("ok, test");
        dict.setLabel("test");
        dict.setParentId("0");
        dict.setValue("13433");
        dict.setIsNewRecord(true);
        dict.setCreateBy(new User("1"));
        dict.setUpdateBy(new User("1"));
        dict.setCreateDate(new Date());
        dict.setUpdateDate(new Date());

        dictService.save(dict);
    }

    @Test
    public void delete() throws Exception {
        dictService.delete(new Dict("test-12345"));
    }


    @Test
    public void deleteList() throws Exception {

    }

    @Test
    public void findList() throws Exception {
        Dict dict = new Dict();
        dict.setType("color");
        List<Dict> dictList = dictService.findList(dict);
        if (dictList != null) {
            logger.info("query list is {}", dictList);
        }
    }

    @Test
    public void findTypeList() throws Exception {

    }

    @Test
    public void findPage() throws Exception {

        Page<Dict> page = new Page<>();
        Page<Dict> dictList = dictService.findPage(page, new Dict());
        if (dictList != null) {
            logger.info("query page list is {}", dictList);
        }
    }

}