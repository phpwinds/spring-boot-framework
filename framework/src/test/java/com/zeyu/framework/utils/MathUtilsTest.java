package com.zeyu.framework.utils;

import org.junit.Test;

/**
 * math 测试
 * Created by zeyuphoenix on 16/7/21.
 */
public class MathUtilsTest {


    @Test
    public void test() {
        System.out.println(MathUtils.toPercent(1.0 / 3.0, 2));
        System.out.println(MathUtils.round(1.0 / 3.0, 2));
        System.out.println(MathUtils.round(1.00000, 2));
        System.out.println(MathUtils.round(1, 2));
    }
}