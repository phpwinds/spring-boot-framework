package com.zeyu.framework.utils.dynacompile;

import com.zeyu.framework.utils.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

/**
 * 动态编译测试
 * Created by zeyuphoenix on 16/9/2.
 */
public class DynamicEngineTest {

    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(DynamicEngineTest.class);

    @Test
    public void compile() {

        try {

            DynamicEngine engine = DynamicEngine.getInstance();
            URL url = DynamicEngineTest.class.getClassLoader().getResource("SimpleHandler.txt");

            if (url != null) {
                String path = url.getPath();

                String code = FileUtils.readFileToString(new File(path), "UTF-8");

                Handler handler = (Handler) engine.compile("SimpleHandler", code);
                handler.invoke("动态编译技术");
            }
            url = DynamicEngineTest.class.getClassLoader().getResource("PackageHandler.txt");
            if (url != null) {
                String path = url.getPath();

                String code = FileUtils.readFileToString(new File(path), "UTF-8");

                Handler handler = (Handler) engine.compile("com.zeyu.framework.tools.PackageHandler", code);
                handler.invoke("动态编译生成UUID");
            }

            String packageName = "com.zeyu.framework.tools";
            String className = "SourceHandler";
            String source = "System.out.println(\"invoke : \" + info + \" id: \" + IdGen.uuid());";

            String code = sourceTempalte(packageName, className, source);
            Handler handler = (Handler) engine.compile(packageName + "." + className, code);
            handler.invoke("动态编译,通过code生成UUID");

        } catch (Exception e) {
            logger.error("compile error: ", e);

        }
    }


    private String sourceTempalte(String packageName, String className, String code) {
        StringBuilder builder = new StringBuilder("");

        builder.append("package ").append(packageName).append(";");
        builder.append("");
        builder.append("import com.zeyu.framework.utils.dynacompile.Handler;");
        builder.append("import com.zeyu.framework.utils.IdGen;");
        builder.append("");
        builder.append("public class ").append(className).append(" implements Handler {");
        builder.append("");
        builder.append("    @Override");
        builder.append("    public void invoke(String info) {");
        builder.append("        ").append(code);
        builder.append("    }");
        builder.append("");
        builder.append("}");

        return builder.toString();
    }
}