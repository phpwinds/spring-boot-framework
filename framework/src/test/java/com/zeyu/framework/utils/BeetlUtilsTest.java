package com.zeyu.framework.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * 测试beetl模板
 * Created by zeyuphoenix on 16/7/19.
 */
public class BeetlUtilsTest {


    /**
     * logger
     */
    private static final Logger logger = LoggerFactory.getLogger(BeetlUtilsTest.class);

    @Test
    public void testRender() {
        // renderString
        Map<String, String> model = Maps.newHashMap();
        model.put("userName", "calvin");
        String result = BeetlUtils.renderString("hello ${userName} <!--: var a = 3; b =a+1; -->  b is ${b} ", model);
        logger.info(result);

        // renderTemplate
        model = Maps.newHashMap();
        model.put("userName", "calvin");
        model.put("password", "123456");
        result = BeetlUtils.renderString("hello ${userName} ${password}", model);
        logger.info(result);

        result = BeetlUtils.renderString(" <!--: var res = commontag.replaceHtml('hello world <p>jone</p>'); --> ${res}  over.", model);
        logger.info(result);
    }

    @Test
    public void root() {
        List<Hz> menuList = Lists.newArrayList();

        // build list
        menuList.add(new Hz("1", "根节点", "0"));

        menuList.add(new Hz("2", "一级菜单1", "1"));
        menuList.add(new Hz("3", "一级菜单2", "1"));
        menuList.add(new Hz("4", "一级菜单3", "1"));

        menuList.add(new Hz("5", "二级1菜单1", "2"));
        menuList.add(new Hz("6", "二级1菜单2", "2"));
        menuList.add(new Hz("7", "二级2菜单1", "3"));
        menuList.add(new Hz("8", "二级2菜单2", "3"));
        menuList.add(new Hz("9", "二级2菜单3", "3"));
        menuList.add(new Hz("10", "二级3菜单1", "4"));
        menuList.add(new Hz("11", "二级3菜单2", "4"));
        menuList.add(new Hz("12", "二级3菜单3", "4"));

        Hz root = null;
        // 获取根节点
        for (Hz menu : menuList) {
            if (StringUtils.equals(menu.getId(), "1")) {
                root = menu;
                break;
            }
        }
        recursion(menuList, root);

        logger.info("{}", root);
    }


    private static void recursion(List<Hz> menuList, Hz root) {
        if (root != null) {
            List<Hz> childList = Lists.newArrayList();
            menuList.stream()
                    .filter(menu -> StringUtils.equals(menu.getPid(), root.getId()))
                    .forEach(menu -> {
                        childList.add(menu);
                        recursion(menuList, menu);
                    });
            root.setChildList(childList);
        }
    }


    /**
     * 测试递归
     */
    private static class Hz {
        private String id;
        private String name;
        private String pid;
        private List<Hz> childList = Lists.newArrayList();

        public Hz(String id, String name, String pid) {
            this.id = id;
            this.name = name;
            this.pid = pid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public List<Hz> getChildList() {
            return childList;
        }

        @Override
        public String toString() {
            return "Hz{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", pid='" + pid + '\'' +
                    ", childList=" + childList +
                    '}';
        }

        public void setChildList(List<Hz> childList) {
            this.childList = childList;
        }


    }
}