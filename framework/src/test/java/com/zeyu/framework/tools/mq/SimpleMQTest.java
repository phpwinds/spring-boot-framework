package com.zeyu.framework.tools.mq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.junit.Test;

import java.io.IOException;

/**
 * 测试mq在公网是否拦截
 * Created by zeyuphoenix on 2017/4/1.
 */
public class SimpleMQTest {

    // ================================================================
    // Constants
    // ================================================================

    // ================================================================
    // Fields
    // ================================================================

    // ================================================================
    // Constructors
    // ================================================================

    // ================================================================
    // Methods from/for super Interfaces or SuperClass
    // ================================================================

    // ================================================================
    // Public or Protected Methods
    // ================================================================

    @Test
    public void sender() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("111.160.225.58");
        factory.setPort(251);
        factory.setUsername("zeyu");
        factory.setPassword("123asd456");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();
        System.out.println("channel created");

        channel.queueDeclare("zeyu-test-queue", true, false, false, null);

        channel.basicPublish("", "zeyu-test-queue", null, "测试外网消息".getBytes());

        channel.close();
        connection.close();
    }

    @Test
    public void receiver() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("111.160.225.58");
        factory.setPort(251);
        factory.setUsername("zeyu");
        factory.setPassword("123asd456");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();
        System.out.println("channel created");

        channel.queueDeclare("zeyu-test-queue", true, false, false, null);

        channel.basicConsume("zeyu-test-queue", new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(message);
            }
        });
Thread.sleep(10000);
        channel.close();
        connection.close();
    }

    // ================================================================
    // Getter & Setter
    // ================================================================

    // ================================================================
    // Private Methods
    // ================================================================

    // ================================================================
    // Inner or Anonymous Class
    // ================================================================

    // ================================================================
    // Test Methods
    // ================================================================

}
