package generate.dao;

import generate.entity.TbAutoTest;
import tk.mybatis.mapper.common.Mapper;

public interface TbAutoTestMapper extends Mapper<TbAutoTest> {
}